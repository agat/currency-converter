import styled from 'styled-components';

interface PropsI {
    
}

const Input = styled.input<PropsI>`
    display: block;
    width: 100%;
    outline: 0;
    height: 56px;
    line-height: 56px;
    font-size: 20px;
    border-radius: 4px;
    padding: 0 16px;
    border: 2px solid #ccc;
    box-sizing: border-box;

    &:focus,
    &:hover {
        border-color: #000;
    }
`;

export default Input;
