import styled from 'styled-components';


export const Container = styled.div`
    padding: 24px;
    max-width: 640px;
    margin: auto;
`;

export default Container;