import styled from 'styled-components';


const Row = styled.div`
    margin-top: 24px;
    
    &:first-child {
        margin: 0;
    }
`;

export default Row;
