import styled from 'styled-components';


export const Headline = styled.h2`
    color: inherit;
    font-size: 24px;
    line-height: 32px;
    font-weight: 400;
    margin: 0;
`;

export const Text = styled.p`
    font-size: 14px;
    line-height: 24px;
    margin: 0;
`;

export default {
    Headline,
    Text
};