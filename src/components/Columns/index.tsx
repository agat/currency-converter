import styled from 'styled-components';


export const Columns = styled.div`
    display: flex;
    align-items: center;
`;


interface ColPropsI {
    isNarrow?: boolean;
}

export const Col = styled.div<ColPropsI>`
    flex: ${props => props.isNarrow ? 'none' : '1'};
    padding-right: 16px;

    &:last-child {
        padding-right: 0;
    }
`;

export default {
    Columns,
    Col
};
