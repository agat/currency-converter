import React from 'react';
import { useFormState } from 'react-use-form-state';

import Button from 'components/Button';
import Container from 'components/Container';
import Row from 'components/Row';
import { Headline } from 'components/Typography';
import Input from 'components/Input';
import { Columns, Col } from 'components/Columns';

enum currency {
    USD = 'USD',
    BYN = 'BYN'
};

interface RatesI {
    [propName: string]: {
        [propName: string]: number
    }
};

const rates: RatesI = {
    'USD': {
        'BYN': 2.0769
    },
    'BYN': {
        'USD': 1/2.0769
    }
};

interface FormI {
    sell: string,
    buy: string,
    from: currency,
    to: currency
};

const initialFormState = {
    sell: '',
    buy: '',
    from: currency.USD,
    to: currency.BYN
};

const formatCurrency = (value: number): string => value.toFixed(4);

const App = () => {
    const [formState, { number }] = useFormState<FormI>(initialFormState, {
        onChange: (e, values) => {
            const { name, value } = e.target;
            
            calculate(name, value, values.from, values.to);
        }
    });
    const calculate = (name: string, value: string, from: string, to: string) => {
        const amount = parseFloat(value);

        if (name === 'sell') {
            formState.setField('buy', formatCurrency(amount * rates[from][to]));
        } else {
            formState.setField('sell', formatCurrency(amount * rates[to][from]));
        }
    };
    const handleSwapClick = () => {
        const {
            sell
        } = formState.values;
        let from = formState.values.to;
        let to = formState.values.from;

        formState.setField('from', from);
        formState.setField('to', to);

        calculate('sell', sell, from, to);
    };

    return (
        <Container>
            <Row>
                <Headline>
                    Конвертер валют
                </Headline>
            </Row>
            <Row>
                <Columns>
                    <Col>
                        <Row>
                            <Columns>
                                <Col>
                                    <Input
                                        placeholder="Продажа"
                                        type="number"
                                        required
                                        min="0"
                                        autoFocus
                                        {...number('sell')}
                                    />
                                </Col>
                                <Col isNarrow>
                                    <Headline>
                                        {formState.values.from}
                                    </Headline>
                                </Col>
                            </Columns>
                        </Row>
                        <Row>
                            <Columns>
                                <Col>
                                    <Input
                                        placeholder="Покупка"
                                        type="number"
                                        required
                                        min="0"
                                        {...number('buy')}
                                    />
                                </Col>
                                <Col isNarrow>
                                    <Headline>
                                        {formState.values.to}
                                    </Headline>
                                </Col>
                            </Columns>
                        </Row>
                    </Col>
                    <Col isNarrow>
                        <Button
                            onClick={handleSwapClick}
                        >
                            Поменять
                        </Button>
                    </Col>
                </Columns>
            </Row>
        </Container>
    );
};

export default App;